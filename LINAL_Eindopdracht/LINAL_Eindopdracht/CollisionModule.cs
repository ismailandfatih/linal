﻿using LINAL_Eindopdracht.Utils;
using LINAL_Eindopdracht.Utils._3D;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINAL_Eindopdracht
{
    class CollisionModule
    {
        public static bool CollisionWithEnemy(List<int[]> boxPlanes, Matrix matrix, Vector3 a, Vector3 b)
        {
            var centerBox = matrix.GetCenter();
            List<Plane3> planes = new List<Plane3>();
            foreach (var boxPlane in boxPlanes)
            {
                Vector3[] vectors = new Vector3[4];
                for (int i = 0; i < 4; i++)
                {
                    double x = matrix.ElementAt(0, boxPlane[i]);
                    double y = matrix.ElementAt(1, boxPlane[i]);
                    double z = matrix.ElementAt(2, boxPlane[i]);
                    vectors[i] = new Vector3(x, y, z);
                }

                planes.Add(new Plane3(vectors[0], vectors[1], vectors[2]));
            }

            foreach (var plane in planes)
            {
                var intersectPoint = plane.InsersectLine(a, b);
                if(intersectPoint.Z < 0)
                {
                    if (intersectPoint.X >= centerBox.X - 50
                        && intersectPoint.X <= centerBox.X + 50

                        && intersectPoint.Y >= centerBox.Y - 50
                        && intersectPoint.Y <= centerBox.Y + 50)
                    {
                        Console.WriteLine("Collided");
                    }

                    foreach (var boxPlane in boxPlanes)
                    {
                        Vector3[] vectors = new Vector3[4];
                        for (int i = 0; i < 4; i++)
                        {
                            double x = matrix.ElementAt(0, boxPlane[i]);
                            double y = matrix.ElementAt(1, boxPlane[i]);
                            double z = matrix.ElementAt(2, boxPlane[i]);
                            vectors[i] = new Vector3(x, y, z);
                        }

                        if(intersectPoint.X >= vectors[0].X
                            && intersectPoint.X >= vectors[3].X
                            && intersectPoint.X <= vectors[1].X
                            && intersectPoint.X <= vectors[2].X

                            && intersectPoint.Y >= vectors[0].Y
                            && intersectPoint.Y >= vectors[1].Y
                            && intersectPoint.Y <= vectors[2].Y
                            && intersectPoint.Y <= vectors[3].Y)
                        {
                            return true;
                            Console.WriteLine("COLLIDED");
                        }
                    }
                }
            }

            return false;
        }
    }
}
