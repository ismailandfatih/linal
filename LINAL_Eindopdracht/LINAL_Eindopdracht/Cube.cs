﻿using LINAL_Eindopdracht.Utils;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINAL_Eindopdracht
{
    class Cube
    {
        public Matrix Matrix { get; set; }
        public List<int[]> Plains { get; set; }

        public Cube()
        {
            // init matrix
            Matrix = new Matrix(3, 8);
            var arr = new double[3, 8]
            {
                { 0, 100, 100,   0,    0,   100,   100,     0 },
                { 0,   0, 100, 100,    0,     0,   100,   100 },
                { 0,   0,   0,   0,  100,   100,   100,   100 }
            };
            Matrix.Copy(arr);

            int[] p1 = new int[4] { 0, 1, 2, 3 };
            int[] p2 = new int[4] { 4, 5, 6, 7 };
            int[] p3 = new int[4] { 0, 4, 7, 3 };
            int[] p4 = new int[4] { 5, 1, 2, 6 };
            int[] p5 = new int[4] { 7, 3, 2, 6 };
            int[] p6 = new int[4] { 0, 4, 5, 1 };

            Plains = new List<int[]>();
            Plains.Add(p1);
            Plains.Add(p2);
            Plains.Add(p3);
            Plains.Add(p4);
            Plains.Add(p5);
            Plains.Add(p6);
        }
    }
}
