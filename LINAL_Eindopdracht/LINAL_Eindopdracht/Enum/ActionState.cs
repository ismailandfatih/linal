﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINAL_Eindopdracht
{
    [Flags]
    public enum ActionState
    {
        None = 0,

        MovingRight = 1,
        MovingLeft = 2,
        MovingUp = 4,
        MovingDown = 8,
        MovingForward = 16,
        MovingBackward = 32,

        CorkSkrewRight = 64,
        CorkSkrewLeft = 128,
        Grow = 256,
        Shrink = 512,
        
        TurnRight = 1024,
        TurnLeft = 2048,

        SkrewR =  4096,
        SkrewL =  8192
        
    }

    public static class ExtensionMethods
    {
        public static bool Is(this ActionState current, ActionState value)
        {
            return (current & value) == value;
        }
    }
    
}
