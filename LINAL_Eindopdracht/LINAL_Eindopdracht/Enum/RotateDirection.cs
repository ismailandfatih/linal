﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINAL_Eindopdracht.Enum
{
    public enum RotateDirection
    {
        Up,
        Down,
        Right,
        Left,
        TurnRight, 
        TurnLeft   
    }
}
