﻿using LINAL_Eindopdracht.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LINAL_Eindopdracht
{
    public partial class Form1 : Form
    {
        GraphicsController graphicsController;
        GameController game;

        public Form1()
        {
            InitializeComponent();

            // Enable panel to double buffer so we wont get flickering
            typeof(Panel).InvokeMember("DoubleBuffered", BindingFlags.SetProperty | BindingFlags.Instance | BindingFlags.NonPublic,
                null, panel3D, new object[] { true });

            // update timer
            Application.Idle += Application_Idle;

            // initialize controller
            graphicsController = new GraphicsController(new Bitmap(panel3D.Bounds.Width, panel3D.Bounds.Height));
            game = new GameController();
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.D)
                game.MoveRight();
            if (e.KeyCode == Keys.A)
                game.MoveLeft();
            if (e.KeyCode == Keys.W)
                game.MoveUp();
            if (e.KeyCode == Keys.S)
                game.MoveDown();

            if (e.KeyCode == Keys.Up)
                game.MoveForward();
            if (e.KeyCode == Keys.Down)
                game.MoveBackward();
            if (e.KeyCode == Keys.Right)
                game.TurnRight();
            if (e.KeyCode == Keys.Left)
                game.TurnLeft();

            if (e.KeyCode == Keys.M)
                game.SkrewR();
            if (e.KeyCode == Keys.N)
                game.SkrewL();

            if (e.KeyCode == Keys.Oemplus)
                game.Grow();
            if (e.KeyCode == Keys.OemMinus)
                game.Shrink();
            if (e.KeyCode == Keys.Q)
                game.CorkSkrewLeft();
            if (e.KeyCode == Keys.E)
                game.CorkSkrewRight();
        }

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.D)
                game.StopMoveRight();
            if (e.KeyCode == Keys.A)
                game.StopMoveLeft();
            if (e.KeyCode == Keys.W)
                game.StopMoveUp();
            if (e.KeyCode == Keys.S)
                game.StopMoveDown();

            if (e.KeyCode == Keys.Up)
                game.StopMoveForward();
            if (e.KeyCode == Keys.Down)
                game.StopMoveBackward();
            if (e.KeyCode == Keys.Right)
                game.StopTurnRight();
            if (e.KeyCode == Keys.Left)
                game.StopTurnLeft();

            if (e.KeyCode == Keys.M)
                game.StopSkrewR();
            if (e.KeyCode == Keys.N)
                game.StopSkrewL();

            if (e.KeyCode == Keys.Oemplus)
                game.StopGrow();
            if (e.KeyCode == Keys.OemMinus)
                game.StopShrink();
            if (e.KeyCode == Keys.Q)
                game.StopCorkSkrewLeft();
            if (e.KeyCode == Keys.E)
                game.StopCorkSkrewRight();
        }

        private void Application_Idle(object sender, EventArgs e)
        {
            // update game and redraw panel
            game.Update();
            panel3D.Invalidate();
        }

        private void Panel3D_Paint(object sender, PaintEventArgs e)
        {
            // draw bitmap
            Graphics g = e.Graphics;
            graphicsController.Draw(game);
            g.DrawImage(graphicsController.getBitmap(), 0, 0);
        }
    }
}
