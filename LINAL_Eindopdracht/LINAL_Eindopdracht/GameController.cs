﻿using LINAL_Eindopdracht.Utils;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LINAL_Eindopdracht.GameObject;
using go = LINAL_Eindopdracht.GameObject;


namespace LINAL_Eindopdracht
{
    class GameController
    {
        // game objects
        go.GameObject mainShip;
        go.GameObject enemyShip;

        // controllers
        MatrixController SpaceShipController;
        MatrixController EnemyShipController;

        // user input
        ActionState action;

        public SpaceShip2 MainShip { get; private set; }

        public Vector3 ShipDirection
        {
            get
            {
                var matrix = SpaceShipController.getMatrix();
                var lastColumn = matrix.ColumnLength;
                var r = new Vector3(
                matrix.ElementAt(0, lastColumn - 2),
                matrix.ElementAt(1, lastColumn - 2),
                matrix.ElementAt(2, lastColumn - 2));

                r += matrix.ToOrigin();

                return Vector3.Normalize(r);
            }
        }
        public Vector3 Nose
        {
            get
            {
                var matrix = SpaceShipController.getMatrix();
                return new Vector3(matrix.ElementAt(0, 0), 
                    matrix.ElementAt(1, 0),
                    matrix.ElementAt(2, 0));
            }
        }

        public GameController()
        {
            // init spaceship
            MainShip = new SpaceShip2();
            SpaceShipController = new MatrixController(MainShip.Matrix);

            // init enemyship
            enemyShip = new EnemyShip();
            EnemyShipController = new MatrixController(enemyShip.Matrix);


            //  translate to position in world
            MainShip.Matrix.Translate(new Vector3(0, 0, -200));
            enemyShip.Matrix.Translate(new Vector3(0, 0, -400));
        }
        
        public Matrix getMainShipMatrix() {
            return SpaceShipController.getMatrix();
        }

        public List<int[]> getMainShipPlanes() {
            return MainShip.Planes;
        }

        public Matrix getEnemyShipMatrix() {
            return EnemyShipController.getMatrix();
        }

        public List<int[]> getEnemyShipPlanes() {
            return enemyShip.Planes;
        }

        public void Update()
        {
            UpdateMainShip();
            updateEnemyShip();
            CollisionModule.CollisionWithEnemy(getEnemyShipPlanes(), getEnemyShipMatrix(), Nose, Nose + ShipDirection * 200D);
        }

        public void UpdateMainShip()
        {
            if (action.Is(ActionState.MovingRight))
            {
                SpaceShipController.Translate(Enum.Translate.X, true);
            }
            if (action.Is(ActionState.MovingLeft))
            {
                SpaceShipController.Translate(Enum.Translate.X, false);
            }
            if (action.Is(ActionState.MovingUp))
            {
                SpaceShipController.Translate(Enum.Translate.Y, true);
            }
            if (action.Is(ActionState.MovingDown))
            {
                SpaceShipController.Translate(Enum.Translate.Y, false);
            }

            if (action.Is(ActionState.MovingForward))
            {
                SpaceShipController.Translate(Enum.Translate.Z, false);
            }
            if (action.Is(ActionState.MovingBackward))
            {
                SpaceShipController.Translate(Enum.Translate.Z, true);
            }
            if (action.Is(ActionState.TurnRight))
            {
                SpaceShipController.Rotate(0.01, Enum.RotateDirection.Right);
            }
            if (action.Is(ActionState.TurnLeft))
            {
                SpaceShipController.Rotate(-0.01, Enum.RotateDirection.Left);
            }

            if (action.Is(ActionState.Grow))
            {
                SpaceShipController.Scale(true);
            }
            if (action.Is(ActionState.Shrink))
            {
                SpaceShipController.Scale(false);
            }
            if (action.Is(ActionState.CorkSkrewRight))
            {
                SpaceShipController.Rotate(-0.01, Enum.RotateDirection.TurnRight);
            }
            if (action.Is(ActionState.CorkSkrewLeft))
            {
                SpaceShipController.Rotate(0.01, Enum.RotateDirection.TurnLeft);
            }
        }

        int counter = 0;
        public void updateEnemyShip()
        {
            counter++;
            if (counter < 100)
            {
                EnemyShipController.Translate(Enum.Translate.Z, false);
            }
            else
            {
                EnemyShipController.Translate(Enum.Translate.Z, true);
                if (counter > 300)
                    counter = -100;
            }

            EnemyShipController.Rotate(0.005, Enum.RotateDirection.Left);

        }

        #region Controls

        public void Grow()
        {
            // set grow
            action |= ActionState.Grow;

            // unset shrink
            action &= ~ActionState.Shrink;
        }

        public void StopGrow()
        {
            action &= ~ActionState.Grow;
        }

        public void Shrink()
        {
            // set shrink
            action |= ActionState.Shrink;

            // unset shrink
            action &= ~ActionState.Grow;
        }

        public void StopShrink()
        {
            action &= ~ActionState.Shrink;
        }

        public void MoveRight()
        {
            // set moving right
            action |= ActionState.MovingRight;

            // unset moving left
            action &= ~ActionState.MovingLeft;
        }
        public void StopMoveRight()
        {
            action &= ~ActionState.MovingRight;
        }

        public void MoveLeft()
        {
            // set moving right
            action |= ActionState.MovingLeft;

            // unset moving left
            action &= ~ActionState.MovingRight;
        }

        public void StopMoveLeft()
        {
            action &= ~ActionState.MovingLeft;
        }

        public void MoveUp()
        {
            // set moving up
            action |= ActionState.MovingUp;

            // unset moving down
            action &= ~ActionState.MovingDown;
        }
        public void StopMoveUp()
        {
            action &= ~ActionState.MovingUp;
        }

        public void MoveDown()
        {
            // set moving down
            action |= ActionState.MovingDown;

            // unset moving up
            action &= ~ActionState.MovingUp;
        }

        public void StopMoveDown()
        {
            action &= ~ActionState.MovingDown;
        }

        public void MoveForward()
        {
            // set moving forward
            action |= ActionState.MovingForward;

            // unset moving backward
            action &= ~ActionState.MovingBackward;
        }
        public void StopMoveForward()
        {
            action &= ~ActionState.MovingForward;
        }

        public void MoveBackward()
        {
            // set moving backward
            action |= ActionState.MovingBackward;

            // unset moving upward
            action &= ~ActionState.MovingForward;
        }

        public void StopMoveBackward()
        {
            action &= ~ActionState.MovingBackward;
        }

        public void TurnRight()
        {
            // set moving backward
            action |= ActionState.TurnRight;

            // unset moving upward
            action &= ~ActionState.TurnLeft;
        }

        public void StopTurnRight()
        {
            action &= ~ActionState.TurnRight;
        }

        public void TurnLeft()
        {
            // set moving backward
            action |= ActionState.TurnLeft;

            // unset moving upward
            action &= ~ActionState.TurnRight;
        }

        public void StopTurnLeft()
        {
            action &= ~ActionState.TurnLeft;
        }

        public void CorkSkrewRight()
        {
            // set moving skrewright
            action |= ActionState.CorkSkrewRight;

            // unset moving skrewleft
            action &= ~ActionState.CorkSkrewLeft;
        }

        public void StopCorkSkrewRight()
        {
            action &= ~ActionState.CorkSkrewRight;
        }

        public void CorkSkrewLeft()
        {
            // set moving skrewleft
            action |= ActionState.CorkSkrewLeft;

            // unset moving skrewright
            action &= ~ActionState.CorkSkrewRight;
        }

        public void StopCorkSkrewLeft()
        {
            action &= ~ActionState.CorkSkrewLeft;
        }

        public void SkrewR()
        {
            // set grow
            action |= ActionState.SkrewR;

            // unset shrink
            action &= ~ActionState.SkrewL;
        }

        public void StopSkrewR()
        {
            action &= ~ActionState.SkrewR;
        }

        public void SkrewL()
        {
            // set grow
            action |= ActionState.SkrewL;

            // unset shrink
            action &= ~ActionState.SkrewR;
        }

        public void StopSkrewL()
        {
            action &= ~ActionState.SkrewL;
        }


        #endregion
    }
}
