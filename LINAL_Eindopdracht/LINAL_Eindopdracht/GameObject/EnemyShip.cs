﻿using LINAL_Eindopdracht.Utils;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINAL_Eindopdracht.GameObject
{
    public class EnemyShip : GameObject
    {
        public EnemyShip()
        {
            // init matrix
            Matrix = new Matrix(3, 9);
            var arr = new double[3, 9]
            {
                { -50,  50,  50, -50, -50,  50, 50,-50, 0 },
                { -50, -50,  50,  50, -50, -50, 50, 50, 0 },
                { -50, -50, -50, -50,  50,  50, 50, 50, 0 }
            };
            Matrix.Copy(arr);

            // init plains of the matrix
            // the integers represent the vector index in the matrix
            int[] p1 = new int[4] { 4, 5, 6, 7 }; // front
            int[] p2 = new int[4] { 5, 1, 2, 6 }; // right side
            int[] p3 = new int[4] { 1, 0, 3, 2 }; // back
            int[] p4 = new int[4] { 0, 4, 7, 3 }; // left side
            int[] p5 = new int[4] { 7, 6, 2, 3 }; // top
            int[] p6 = new int[4] { 0, 1, 5, 4 }; // bottom

            Planes = new List<int[]>();
            Planes.Add(p1);
            Planes.Add(p2);
            Planes.Add(p3);
            Planes.Add(p4);
            Planes.Add(p5);
            Planes.Add(p6);
        }

    }
}
