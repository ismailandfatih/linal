﻿using LINAL_Eindopdracht.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINAL_Eindopdracht.GameObject
{
    public class GameObject
    {
        public Matrix Matrix { get; set; }
        public List<int[]> Planes { get; set; }
    }
}
