﻿using LINAL_Eindopdracht.Utils;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINAL_Eindopdracht.GameObject
{
    public class SpaceShip : GameObject
    {
        public SpaceShip()
        {
            // init matrix
            Matrix = new Matrix(3, 4);
            var arr = new double[3, 4]
            {
                {    0,  50,  -50,    0 },
                {    0,   0,    0,   40 },
                { -100,  50,   50,  100 }
            };
            Matrix.Copy(arr);

            // init plains of the matrix
            // the integers represent the vector index in the matrix
            int[] Bottom  = new int[3] { 0, 1, 2 };
            int[] Rwindow = new int[3] { 0, 1, 3 };
            int[] Lwindow = new int[3] { 0, 2, 3 };
            int[] Back    = new int[3] { 1, 2, 3 };

            Planes = new List<int[]>();
            Planes.Add(Bottom);
            Planes.Add(Rwindow);
            Planes.Add(Lwindow);
            Planes.Add(Back);
        }

    }
}
