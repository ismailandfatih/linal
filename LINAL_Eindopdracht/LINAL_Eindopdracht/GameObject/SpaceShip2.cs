﻿using LINAL_Eindopdracht.Utils;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINAL_Eindopdracht.GameObject
{
    public class SpaceShip2 : GameObject
    {
        public Vector3 Nose
        {
            get
            {
                return new Vector3(Matrix.ElementAt(0, 0), 
                    Matrix.ElementAt(1, 0),
                    Matrix.ElementAt(2, 0));
            }
        }

        public SpaceShip2()
        {
            // init matrix
            Matrix = new Matrix(3, 20);
            var arr = new double[3, 20]
            {
               // D = Direction vector
               // O = Origin / Center
               // |----------------Middle--------------|--------right wing-------|--------leftwing---------|--D-|-O-|
                {    0,  0, -20, -20,  20,  20, 20, -20,  50,  90,  50,   50,  20, -50, -90, -50,  -50, -20,  0,  0},
                {    0, 15,   0,  20,  20,   0,  0,   0, -15, -15, -15,  -10,  10, -15, -15, -15,  -10,  10,  0,  0},
                { -100,  0, 100, 100, 100, 100,  0,   0, 100, 100,  70,  100, 100, 100, 100,  70,  100, 100, -1,  0}
            };
            Matrix.Copy(arr);

            // init plains of the matrix
            // the integers represent the vector index in the matrix
            int[] p1  = new int[5] { 0, 7, 2, 5, 6 }; // ondervlak
            int[] p2  = new int[4] { 0, 7, 3, 1 }; // linker raam
            int[] p3  = new int[4] { 0, 6, 4, 1 }; // rechterraam
            int[] p4  = new int[4] { 2, 3, 4, 5 }; // achterkant
            int[] p5  = new int[3] { 7, 2, 3}; // linker kant
            int[] p6  = new int[3] { 6, 5, 4 }; // rechterkant
            int[] p7  = new int[3] { 1, 3, 4 }; // bovenkant

            int[] p8  = new int[4] { 6, 5, 8, 10 }; //
            int[] p9  = new int[3] { 8, 9, 10 }; //
            int[] p10  = new int[3] { 11, 9, 10 }; //
            int[] p11  = new int[4] { 6, 12, 11, 10 }; //
            int[] p12  = new int[4] { 12, 11, 8, 5 }; //
            int[] p13  = new int[3] { 11, 9, 8 }; //

            int[] p14 = new int[4] { 7, 2, 13, 15 }; //
            int[] p15 = new int[3] { 15, 13, 14 }; //
            int[] p16 = new int[3] { 15, 16, 14 }; //
            int[] p17 = new int[4] { 7, 17, 16, 15 }; //
            int[] p18 = new int[4] {17, 16, 13, 2 }; //
            int[] p19 = new int[3] { 14, 16, 13 }; //


            Planes = new List<int[]>();
            // body
            Planes.Add(p1);
            Planes.Add(p2);
            Planes.Add(p3);
            Planes.Add(p4);
            Planes.Add(p5);
            Planes.Add(p6);
            Planes.Add(p7);

            // right wing
            Planes.Add(p8);
            Planes.Add(p9);
            Planes.Add(p10);
            Planes.Add(p11);
            Planes.Add(p12);
            Planes.Add(p13);

            // left wing
            Planes.Add(p14);
            Planes.Add(p15);
            Planes.Add(p16);
            Planes.Add(p17);
            Planes.Add(p18);
            Planes.Add(p19);



        }

    }
}
