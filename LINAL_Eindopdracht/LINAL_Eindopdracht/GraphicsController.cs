﻿using LINAL_Eindopdracht.Utils;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINAL_Eindopdracht
{
    class GraphicsController
    {
        // the bitmap we use to draw the objects on
        Bitmap bitmap;
        Image Background;

        // camera metrixes
        Matrix Projection;

        // the camera vector propperties
        Vector3 Eye = new Vector3(0, 0, 0);
        Vector3 Lookat = new Vector3(0, 0, -200);
        Vector3 Up = new Vector3(0, 1, 0);

        // view
        double near = 100;
        double far = 600;
        double fieldOfView = 90;
        double ScreenSize;
            
        public GraphicsController(Bitmap b)
        {
            bitmap = b;
            Background = Image.FromFile("Space.jpg");
            ScreenSize = bitmap.Width;

            InitCamera();
        }
        
        public GraphicsController(Bitmap b, Vector3 eye, Vector3 lookat, Vector3 up)
        {
            bitmap = b;
            Background = Image.FromFile("Space.jpg");

            Eye = eye;
            Lookat = lookat;
            Up = up;
            ScreenSize = bitmap.Width;

            InitCamera();
        }

        private void InitCamera()
        {
            // calculate camera matrix
            Vector3 z = Vector3.Normalize(Eye - Lookat);
            Vector3 y = Vector3.Normalize(Up);
            Vector3 x = Vector3.Normalize(Vector3.Cross(y, z));
            y = Vector3.Normalize(Vector3.Cross(z, x));

            Matrix Camera = new Matrix(4, 4);
            var arr = new double[4, 4]
            {
                { x.X, x.Y, x.Z, -Vector3.Dot(x, Eye)},
                { y.X, y.Y, y.Z, -Vector3.Dot(y, Eye)},
                { z.X, z.Y, z.Z, -Vector3.Dot(z, Eye)},
                { 0  , 0  , 0  ,  1}
            };
            Camera.Copy(arr);
            
            // set screen size
           // ScreenSize = far * 2;

            // calculate perspective projection matrix
            double rad = (fieldOfView * Math.PI / 180.0);
            double scale = near * Math.Tan(rad * 0.5);
            Matrix Perspective = new Matrix(4, 4);
            var parr = new double[4, 4]
            {
                { scale, 0    , 0  ,  0},
                { 0    , scale, 0  ,  0},
                { 0    , 0    , ( -far / (far - near))  ,  -1},
                { 0    , 0    , ((-(far * near)) / (far - near)),0}
            };
            Perspective.Copy(parr);

            // set projection matrix
            Projection = Perspective * Camera;
        }

        private Matrix fixMatrix(Matrix m)
        {
            Matrix temp = new Matrix(m.RowLength, m.ColumnLength);
            
            for (int c = 0; c < m.ColumnLength; c++)
            {
                double x = m.ElementAt(0, c);
                double y = m.ElementAt(1, c);
                double z = m.ElementAt(2, c);
                double w = m.ElementAt(3, c);

                if (w <= 0)
                {
                    w += 1;
                }


                double NewX = (ScreenSize / 2) + ((double)(x + 1) / w) * ScreenSize * 0.5;
                double NewY = (ScreenSize / 2) + ((double)(y + 1) / w) * ScreenSize * 0.5;
                double NewZ = -z;

                temp.Insert(0, c, NewX);
                temp.Insert(1, c, NewY);
                temp.Insert(2, c, NewZ);
                temp.Insert(3, c, m.ElementAt(3, c));
            }

            return temp;
        }

        public Bitmap getBitmap()
        {
            return bitmap;
        }

        public void Draw(GameController gc)
        {
            using (var graphics = Graphics.FromImage(bitmap))
            {
                // clear bitmap
                graphics.DrawImage(Background, 0, 0);
                
                // get mainship
                var ship = gc.MainShip;
                Matrix main = gc.getMainShipMatrix();
                var plains = gc.getMainShipPlanes();
                main.AddRow();

                // get enemyship
                Matrix Enemy = gc.getEnemyShipMatrix();
                var EnemyPlanes = gc.getEnemyShipPlanes();
                Enemy.AddRow();

                // calculate perspectives
                main = fixMatrix(Projection * main);
                Enemy = fixMatrix(Projection * Enemy);

                // calculate Z values of the 2 objects
                Vector3 Vmain = main.GetCenter();
                Vector3 Venemy = Enemy.GetCenter();
                if (Vmain.Z > Venemy.Z)
                {
                    drawMatrix(Enemy, EnemyPlanes, graphics);
                    drawDirectionLine(gc.Nose, gc.ShipDirection, 500, graphics);
                    drawMatrix(main, plains, graphics);
                }
                else
                {
                    drawMatrix(main, plains, graphics);
                    drawDirectionLine(gc.Nose, gc.ShipDirection, 500, graphics);
                    drawMatrix(Enemy, EnemyPlanes, graphics);

                }
                
                main.DelRow();
                Enemy.DelRow();
            }
        }

        private void drawDirectionLine(Vector3 positionVector, Vector3 directionVector, double lambda, Graphics g)
        {
            Vector3 endpoint = positionVector + directionVector * lambda;
            var matrix = new Matrix(3, 2);
            matrix.Copy(
                new double[3, 2]
                {
                    { positionVector.X, endpoint.X },
                    { positionVector.Y, endpoint.Y },
                    { positionVector.Z, endpoint.Z }
                });
            matrix.AddRow();
            matrix = fixMatrix(Projection * matrix);

            var beginDrawV = new Vector3(matrix.ElementAt(0, 0), matrix.ElementAt(1, 0), matrix.ElementAt(2, 0));
            var endDrawV = new Vector3(matrix.ElementAt(0, 1), matrix.ElementAt(1, 1), matrix.ElementAt(2, 1));

            // check if W value is under zero, this means its too close
            if (matrix.ElementAt(3, 0) > 0 && matrix.ElementAt(3, 1) > 0)
            {
                g.DrawLine(Pens.Red, getPoint((int)beginDrawV.X, (int)beginDrawV.Y), getPoint((int)endDrawV.X, (int)endDrawV.Y));
            }
            matrix.DelRow();
        }

        private void drawMatrix(Matrix matrix, List<int[]> planes, Graphics g)
        {
            // loop through all plains
            List<Pair<Point[], double>> vlakken = new List<Pair<Point[], double>>();
            foreach (int[] plane in planes)
            {
                // create plain with the given indexes
                Point[] points = new Point[plane.Length];
                bool draw = true; // indicaties if we are going to draw this plain
                double Zvalue = 0;   // the total Z value of the plain
                for (int i = 0; i < plane.Length; i++)
                {
                    int index = plane[i];
                    // check if W is under zero, then return we stop making this plain
                    if ((int)matrix.ElementAt(3, index) < 0)
                    {
                        draw = false;
                        break;
                    }

                    // get point to draw on screen
                    points[i] = getPoint((int)matrix.ElementAt(0, index), (int)matrix.ElementAt(1, index));
                    Zvalue += matrix.ElementAt(2, index);
                }


                Zvalue /= points.Length;

                // if we can draw add to vlakken list
                if (draw)
                    vlakken.Add(new Pair<Point[], double>(points, Zvalue));
            }

            // sort the vlakken list with the Zvalue
            // now the order of the list draws the farest Z first zo its on the back;
            vlakken.Sort(new ZComparer());

            // draw all plains
            foreach(var vlak in vlakken)
            {
                g.FillPolygon(Brushes.White, vlak.First);
                g.DrawPolygon(Pens.Black, vlak.First);
            }
        }

        private Point getPoint(int x, int y)
        {
            Point point = new Point();

            point.X = x;
            point.Y = Convert.ToInt32(bitmap.Height - (double)y);
          
            return point;
        }
    }
    class ZComparer : IComparer<Pair<Point[], double>>
    {
        public int Compare(Pair<Point[], double> a, Pair<Point[], double> b)
        {
            return a.Second.CompareTo(b.Second);
        }
    }

}
