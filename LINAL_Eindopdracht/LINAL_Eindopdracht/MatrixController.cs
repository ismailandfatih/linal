﻿using LINAL_Eindopdracht.Enum;
using LINAL_Eindopdracht.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINAL_Eindopdracht
{
    public class MatrixController
    {
        // the game object
        Matrix main;

        // values
        double scaleFactor = 1.03;
        double transValue = 1.01;


        // the processing matrices
        Matrix scale;
        bool previousScale = true;
        
        Matrix rotateX;
        Matrix rotateY;
        Matrix rotateZ;
        Matrix rotateSkrew;
        double radian = 0;

        // transleate values
        bool hasMoved = false;
        
        public MatrixController(Matrix main)
        {
            this.main = main;
        }

        public Matrix getMatrix()
        {
            return main;
        }

        public void Scale(bool grow)
        {
            // if main has moved we need to calculate a new scale matrix, else we can use the same scale matrix
            if (hasMoved || scale == null)
            {
                scale = CreateScaleMatrix(scaleFactor, main.ToOrigin());

                // inverse if the previous action was shrink
                if (previousScale == false)
                    scale = Matrix.GetInverseMatrix(scale.Clone());
            }

            // did we chnage the grow, we need to shrink, take inverse
            if (grow != previousScale)
            {
                previousScale = grow;
                scale = Matrix.GetInverseMatrix(scale.Clone());
            }

            // Add row so we can multiply, remove row afterwards
            main.AddRow();
            main = scale * main;
            main.DelRow();
        }

        public void Translate(Translate t, bool positive)
        {
            Vector3 trans = new Vector3(0,0,0);
            switch (t)
            {
                case Enum.Translate.X:
                    trans = new Vector3(transValue, 0, 0);
                    break;
                case Enum.Translate.Y:
                    trans = new Vector3(0, transValue, 0);
                    break;
                case Enum.Translate.Z:
                    trans = new Vector3(0, 0, transValue);
                    break;
                default:
                    break;
            }
            
            // calculate translateion matrix
            Matrix translate = Matrix.GetTranslateMatrix(trans.X, trans.Y, trans.Z);

            // if counterclockwise, we need the inverse
            if (!positive)
            {
                translate = Matrix.GetInverseMatrix(translate.Clone());
            }


            // Add row so we can multiply, remove row afterwards
            main.AddRow();
            main = translate * main;
            main.DelRow();

            // we translated so set hasmoved to true so other functions know they need to recalculate the matrices
            hasMoved = true;
        }

        public void Rotate(double radians, RotateDirection direction)
        {
            // change X, Y ,Z rotate matrices when radian and hasmoved are changed. else use same matrices;
            if (radian != radians || hasMoved)
            {
                CreateRotateMatrix(radians);
            }

            // rotate main matrix on given direction
            Matrix rot = null;
            switch (direction)
            {
                case RotateDirection.Up:
                case RotateDirection.Down:
                    rot = rotateX;
                    break;
                case RotateDirection.Right:
                case RotateDirection.Left:
                    rot = rotateY;
                    break;
                case RotateDirection.TurnRight:
                case RotateDirection.TurnLeft:
                    rot = rotateZ;
                    break;
                default:
                    break;
            }

                      
            // rotate main
            main.AddRow();
            main = rot * main;
            main.DelRow();
        }


        public void RotateSkrew(double radians)
        {
            // change X, Y ,Z rotate matrices when radian and hasmoved are changed. else use same matrices;
            if (radian != radians || hasMoved || rotateSkrew == null)
            {
                CreateRotateSkrewMatrix(radians);
            }
            
            // rotate main
            main.AddRow();
            main = rotateSkrew * main;
            main.DelRow();
        }

        private Matrix CreateScaleMatrix(double factor, Vector3 translation)
        {
            // save value

            // create scale matrix with extra dimension so we can translate
            var scale = Matrix.GetScaleMatrix(factor, factor, factor);
            scale.AddExtraDimension();

            // Create translation and reverse-translation matrix
            var translate = Matrix.GetTranslateMatrix(translation.X, translation.Y, translation.Z);
            var retranslate = Matrix.GetInverseMatrix(translate.Clone());
            
            return retranslate * scale * translate;
        }

        private void CreateRotateMatrix(double radians)
        {
            // get vector to origin and the vector to conter of main
            Vector3 center = main.GetCenter();
            Vector3 origin = main.ToOrigin();

            // calculate transelation matrices
            Matrix trans = Matrix.GetTranslateMatrix(origin.X, origin.Y, origin.Z);
            Matrix retrans = Matrix.GetTranslateMatrix(center.X, center.Y, center.Z);

            // calculate X, Y, Z rotation matrices
            Matrix rotX = Matrix.GetXRotationMatrix(radians);
            Matrix rotY = Matrix.GetYRotationMatrix(radians);
            Matrix rotZ = Matrix.GetZRotationMatrix(radians);

            // define X, Y, Z rotation matrices
            rotX.AddExtraDimension();
            rotY.AddExtraDimension();
            rotZ.AddExtraDimension();

            rotateX = retrans * rotX * trans;
            rotateY = retrans * rotY * trans;
            rotateZ = retrans * rotZ * trans;

            // save values for next iteration
            radian = radians;
            hasMoved = false;
        }

        private void CreateRotateSkrewMatrix(double radians)
        {
           Vector3 origin = main.ToOrigin();
           
           double x = main.ElementAt(0, 0);
           double y = main.ElementAt(1, 0);
           double z = main.ElementAt(2, 0);
           
           float t1 = (float)Math.Atan2(z, x);
           float t2 = (float)Math.Atan2(y, Math.Sqrt(Math.Pow(x, 2) + Math.Pow(z, 2)));
           
           Matrix trans = Matrix.GetTranslateMatrix(origin.X, origin.Y, origin.Z);
           Matrix roty = Matrix.GetYRotationMatrix(t1);
           Matrix rotz = Matrix.GetZRotationMatrix(t2);
           Matrix rot = Matrix.GetXRotationMatrix(radians);
           Matrix rerotz = Matrix.Adjoint(Matrix.GetZRotationMatrix(t2));
           Matrix reroty = Matrix.Adjoint(Matrix.GetYRotationMatrix(t1));
           Matrix retrans = Matrix.GetInverseMatrix(trans.Clone());
           
           roty.AddExtraDimension();
           rotz.AddExtraDimension();
           rot.AddExtraDimension();
           rerotz.AddExtraDimension();
           reroty.AddExtraDimension();
           
           rotateSkrew = retrans * rerotz * reroty * rot * roty * rotz * trans;
           hasMoved = false;
        }


        public void ProcessAllMatrices()
        {

        }

        public void ProcessAllMatrices(double factor, double rotX, double rotY, double rotZ, double tX, double tY, double tZ)
        {

        }
    }
}
