﻿using System;

public struct Vector2
{
    // Properties
    public double X { get; private set; }
    public double Y { get; private set; }
    public double Length { get { return Math.Sqrt(X * X + Y * Y); } }

    // Constructor
    public Vector2(double x, double y)
    {
        X = x;
        Y = y;
    }

    // Addition of vectors
    public static Vector2 operator +(Vector2 v1, Vector2 v2)
    {
        return new Vector2(v1.X + v2.X, v1.Y + v2.Y);
    }

    // Subtraction of vectors
    public static Vector2 operator -(Vector2 v1, Vector2 v2)
    {
        return new Vector2(v1.X - v2.X, v1.Y - v2.Y);
    }

    // Multiplication of vector
    public static Vector2 operator *(Vector2 vector, double factor)
    {
        return new Vector2(vector.X * factor, vector.Y * factor);
    }

    // Division of vector
    public static Vector2 operator /(Vector2 vector, double divisor)
    {
        return new Vector2(vector.X / divisor, vector.Y / divisor);
    }

    // Normalization of vector
    public static Vector2 Normalize(Vector2 vector)
    {
        if(vector.Length == 0)
        {
            return vector;
        }

        return new Vector2(vector.X / vector.Length, vector.Y / vector.Length);
    }

    // Perpendicular vector
    public static Vector2 Perp(Vector2 vector)
    {
        return new Vector2(-vector.Y, vector.X);
    }

    // Distance to a given vector in euclidean space
    public double Distance(Vector2 vector)
    {
        double yDiff = vector.Y - Y;
        double xDiff = vector.X - X;
        return Math.Sqrt(yDiff * yDiff + xDiff * xDiff);
    }

    // Dot product of two vectors
    public double Dot(Vector2 vector)
    {
        return X * vector.X + Y * vector.Y;
    }
}