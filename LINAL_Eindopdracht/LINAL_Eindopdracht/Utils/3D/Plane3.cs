﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINAL_Eindopdracht.Utils._3D
{
    public struct Plane3
    {
        private Vector3 a, b, c;
        public double Distance { get; private set; }
        public Vector3 Normal { get; private set; }

        public Plane3(Vector3 a, Vector3 b, Vector3 c)
        {
            // Points have to be ordered Counter-clockwise
            this.a = a;
            this.b = b;
            this.c = c;

            // Counter-clockwise
            var dir = Vector3.Cross(b - a, c - a);
            Normal = Vector3.Normalize(dir);
            Distance = Vector3.Dot(Normal, a);
        }

        public Vector3 InsersectLine(Vector3 a, Vector3 b)
        {
            /*
            Vector3 ba = b - a;
            double nDotA = Vector3.Dot(Normal, a);
            double nDotBA = Vector3.Dot(Normal, ba);

            return a + (((Distance - nDotA) / nDotBA) * ba);
            */

            double dot1 = Vector3.Dot(Normal, this.a - a);
            double dot2 = Vector3.Dot(Normal, b - a);

            return a + (dot1 / dot2) * (b - a);
        }
    }
}
