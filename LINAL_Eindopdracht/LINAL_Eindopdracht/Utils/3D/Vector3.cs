﻿using System;

public struct Vector3
{
    // Properties
    public double X { get; private set; }
    public double Y { get; private set; }
    public double Z { get; private set; }
    public double Length { get { return Math.Sqrt(X * X + Y * Y + Z * Z); } }

    // Constructor
    public Vector3(double x, double y, double z)
    {
        X = x;
        Y = y;
        Z = z;
    }

    // Addition of vectors
    public static Vector3 operator +(Vector3 v1, Vector3 v2)
    {
        return new Vector3(v1.X + v2.X, v1.Y + v2.Y, v1.Z + v2.Z);
    }

    // Subtraction of vectors
    public static Vector3 operator -(Vector3 v1, Vector3 v2)
    {
        return new Vector3(v1.X - v2.X, v1.Y - v2.Y, v1.Z - v2.Z);
    }

    // Multiplication of vector
    public static Vector3 operator *(Vector3 vector, double factor)
    {
        return new Vector3(vector.X * factor, vector.Y * factor, vector.Z * factor);
    }

    public static Vector3 operator *(double factor, Vector3 vector)
    {
        return new Vector3(vector.X * factor, vector.Y * factor, vector.Z * factor);
    }

    // Division of vector
    public static Vector3 operator /(Vector3 vector, double divisor)
    {
        return new Vector3(vector.X / divisor, vector.Y / divisor, vector.Z / divisor);
    }

    // Normalization of vector
    public static Vector3 Normalize(Vector3 vector)
    {
        if(vector.Length == 0)
        {
            return vector;
        }

        return new Vector3(vector.X / vector.Length, vector.Y / vector.Length, vector.Z / vector.Length);
    }

    // Perpendicular vector
    public static Vector3 Perp(Vector3 vector)
    {
        /* 
         * Steps to find any perpendicular 3D vector
         * 1) Create two 2D vectors containing x,y and x,z
         * 2) Calculate the perpendicular vectors of those vectors
         * 3) Evaluate the following linear formula to obtain the perpendicular 3D coordinates
         *    vector = ((x1*a + x2*b)+yb+zb where a,b is any real number
         *    
         *    x = ((x1*a + x2*b)
         *    y = yb
         *    z = zb
         * 4) Create a 3D vector using these coordinates
         */

        // Take x and y coordinates to create a 2D vector 
        var xyVector = new Vector2(vector.X, vector.Y);
        // Any perpendicular vector will do
        xyVector = Vector2.Perp(xyVector);

        // Take x and z coordinates to create a 2D vector
        var xzVector = new Vector2(vector.X, vector.Z);
        // Any perpendicular vector will do
        xzVector = Vector2.Perp(xzVector);
        
        double a = 1D, b = 2D;                      // Both a and b should be any real number
        double x = xyVector.X * a + xzVector.X * b;
        double y = xyVector.Y * a;
        double z = xzVector.Y * b;                  // The Y property will return the Z value of the second perpendicar 2D vector 

        return new Vector3(x, y, z);
    }

    // Distance to a given vector in 3D euclidean space
    public double Distance(Vector3 vector)
    {
        double xDiff = vector.X - X;
        double yDiff = vector.Y - Y;
        double zDiff = vector.Z - Z;
        return Math.Sqrt(xDiff * xDiff + yDiff * yDiff + zDiff * zDiff);
    }

    // Dot / inner product of two vectors
    public static double Dot(Vector3 a, Vector3 b)
    {
        return a.X * b.X + a.Y * b.Y + a.Z * a.Z;
    }

    // The cross product of 2 vectors
    // Returns the perpendicular vector to the 2 vectors
    public static Vector3 Cross(Vector3 a, Vector3 b)
    {
        double cX = (a.Y * b.Z) - (b.Y * a.Z);
        double cY = (b.X * a.Z) - (a.X * b.Z);
        double cZ = (a.X * b.Y) - (b.X * a.Y);

        return new Vector3(cX, cY, cZ);
    }

    // Returns te angle of 2 vectors
    public static double AngleOf(Vector3 a, Vector3 b)
    {
        // Dot product / length vec a * lenght vec b
        return Vector3.Dot(a, b) / (a.Length * b.Length);
    }

    // Checks if it has a 90 or 270 degree angle to the given vector
    public bool HasRightAngleWith(Vector3 v)
    {
        return AngleOf(this, v) == 0;
    }

    // Checks if the given vector is independant
    // The vector is dependant if between the 2 vectors are 0 or 180 decrease
    public bool IsIndependantTo(Vector3 v)
    {
        double angle = Vector3.Cross(this, v).Length / (this.Length * v.Length);
        return angle != 0 || angle != 180;
    }


    // Checks if the given point is in the plane given with the 3 vectors
    // baseV = steunvector
    // a / b = richtingsvector
    // Returns false if the 2 vectors are not independant
    public static bool IsPointInPlane(int x, int y, int z, Vector3 baseV, Vector3 a, Vector3 b)
    {
        if (!a.IsIndependantTo(b))
            return false;

        Vector3 crossV = Cross(a, b);

        // if the dot product of the crossvector and basevector is equal to -15, the point is in the plane.
        return Vector3.Dot(crossV, baseV) == -15;
    }
}