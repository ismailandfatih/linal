﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINAL_Eindopdracht.Utils
{
    public enum MatrixType
    {
        IDENTITY,
        UNITY,
        NULL
    }

    public class Matrix
    {
        private double[,] matrix;

        public int RowLength { get { return matrix.GetLength(0); } }
        public int ColumnLength { get { return matrix.GetLength(1); } }

        public Matrix(int rowLength, int columnLength)
        {
            matrix = new double[rowLength, columnLength];
        }

        public Matrix(MatrixType matrixType, int dimension)
        {
            switch (matrixType)
            {
                case MatrixType.IDENTITY:
                    matrix = new double[2, 2];
                    matrix[0, 0] = 1D;
                    matrix[1, 1] = 1D;
                    break;
                case MatrixType.UNITY:
                    matrix = new double[dimension, dimension];
                    for (int i = 0; i < dimension; i++)
                    {
                        matrix[i, i] = 1D;
                    }
                    break;
                case MatrixType.NULL:
                    matrix = new double[dimension, dimension];
                    break;
            }
        }

        public void Insert(int row, int column, double element)
        {
            if (row <= matrix.GetLength(0) && column <= matrix.GetLength(1))
            {
                matrix[row, column] = element;
            }
        }
        public void Copy(double[,] inMatrix)
        {
            if(matrix.Length != inMatrix.Length)
            {
                return;
            }

            matrix = inMatrix;
        }

        public double ElementAt(int row, int column)
        {
            return matrix[row, column];
        }

        public void Scale(double factor, Vector3 translation)
        {
            // create scale matrix with extra dimension so we can translate
            var scale = Matrix.GetScaleMatrix(factor, factor, factor);
            scale.AddExtraDimension();

            // Create translation and reverse-translation matrix
            var translate = Matrix.GetTranslateMatrix(translation.X, translation.Y, translation.Z);
            var retranslate = Matrix.GetTranslateMatrix(translation.X * -1, translation.Y * -1, translation.Z * -1);

            // Add row so we can translate, remove row afterwards
            this.AddRow();
            var result = retranslate * scale * translate * this;
            result.DelRow();
 
            matrix = result.matrix;
        }

        public void Translate(Vector3 translation)
        {
            // Create translation matrix to given vector location
            var translate = Matrix.GetTranslateMatrix(translation.X, translation.Y, translation.Z);

            // Add row so we can translate, remove row afterwards
            this.AddRow();
            var result = translate * this;
            result.DelRow();

            matrix = result.matrix;
        }

        public void Rotate(double xRad, double yRad, double zRad)
        {
            var xrm = Matrix.GetXRotationMatrix(xRad);
            var yrm = Matrix.GetYRotationMatrix(yRad);
            var zrm = Matrix.GetZRotationMatrix(zRad);

            var result = xrm * yrm * zrm * this;
            matrix = result.matrix;
        }

        public Vector3 GetCenter()
        {
            var lastColumn = ColumnLength - 1;
            return new Vector3(matrix[0, lastColumn], matrix[1, lastColumn], matrix[2, lastColumn]);
            /*
            double minX = matrix[0, 0];
            double minY = matrix[1, 0];
            double minZ = matrix[2, 0];

            double maxX = minX;
            double maxY = minY;
            double maxZ = minZ;

            for (int c = 0; c < ColumnLength; c++)
            {
                double mx = matrix[0, c];
                double my = matrix[1, c];
                double mz = matrix[2, c];

                if (mx < minX)
                    minX = mx;
                if (my < minY)
                    minY = my;
                if (mz < minZ)
                    minZ = mz;

                if (mx > maxX)
                    maxX = mx;
                if (my > maxY)
                    maxY = my;
                if (mz > maxZ)
                    maxZ = mz;
            }

            double x = minX + (maxX - minX) / 2;
            double y = minY + (maxY - minY) / 2;
            double z = minZ + (maxZ - minZ) / 2;
            return new Vector3(x, y, z);
            */
        }

        public Vector3 ToOrigin()
        {
            Vector3 center = GetCenter();
            return new Vector3(center.X * -1, center.Y * -1, center.Z * -1);
        }

        public static Matrix operator *(Matrix m1, Matrix m2)
        {
            int rowLength = m1.RowLength;
            int columnLength = m2.ColumnLength;
            var returnMatrix = new Matrix(rowLength, columnLength);

            for (int k = 0; k < columnLength; k++)
            {
                for (int i = 0; i < m1.RowLength; i++)
                {
                    double result = 0D;
                    for (int j = 0; j < m1.ColumnLength; j++)
                    {
                        double element1 = m1.ElementAt(i, j);
                        double element2 = m2.ElementAt(j, k);
                        result += element1 * element2;
                    }
                    returnMatrix.Insert(i, k, result);
                }
            }

            return returnMatrix;
        }
        public static Matrix operator +(Matrix m1, Matrix m2)
        {
            int rowLength = m1.RowLength;
            int columnLength = m2.ColumnLength;
            var returnMatrix = new Matrix(rowLength, columnLength);


            for (int r = 0; r < rowLength; r++)
            {
                for (int c = 0; c < columnLength; c++)
                {
                    returnMatrix.Insert(r, c, (m1.ElementAt(r, c) + m2.ElementAt(r, c)));
                }
            }

            return returnMatrix;
        }

        public static Matrix GetXRotationMatrix(double radians)
        {
            var xrm = new Matrix(3, 3);
            var arr = new double[3, 3] 
            { 
                { 1, 0, 0 }, 
                { 0, Math.Cos(radians), -Math.Sin(radians) },
                { 0, Math.Sin(radians), Math.Cos(radians) }
            };
            xrm.Copy(arr);

            return xrm;
        }
        public static Matrix GetYRotationMatrix(double radians)
        {
            var yrm = new Matrix(3, 3);
            var arr = new double[3, 3] 
            { 
                { Math.Cos(radians), 0, -Math.Sin(radians) }, 
                { 0, 1, 0 },
                { Math.Sin(radians), 0, Math.Cos(radians) }
            };
            yrm.Copy(arr);

            return yrm;
        }
        public static Matrix GetZRotationMatrix(double radians)
        {
            var zrm = new Matrix(3, 3);
            var arr = new double[3, 3] 
            { 
                { Math.Cos(radians), -Math.Sin(radians), 0 }, 
                { Math.Sin(radians), Math.Cos(radians), 0 },
                { 0, 0, 1 }
            };
            zrm.Copy(arr);

            return zrm;
        }

        public static Matrix GetScaleMatrix(double factorX, double factorY, double factorZ)
        {
            var scale = new Matrix(3, 3);
            var arr = new double[3, 3]
            {
                { factorX,  0,       0 },
                { 0,        factorY, 0 },
                { 0,        0,       factorZ }
            };
            scale.Copy(arr);

            return scale;
        }
        public static Matrix GetTranslateMatrix(double tX, double tY, double tZ)
        {
            var translate = new Matrix(4, 4);
            var arr = new double[4, 4]
            {
                { 1, 0 ,0 ,tX},
                { 0, 1 ,0 ,tY},
                { 0, 0 ,1 ,tZ},
                { 0, 0 ,0 ,1},
            };
            translate.Copy(arr);

            return translate;
        }

        public static Matrix Adjoint(Matrix m)
        {
            if (!m.isSquared())
                return m;

            // flip all values over the diagonal line [left top to right bottom]
            Matrix temp = new Matrix(m.RowLength, m.ColumnLength);
            for (int r = 0; r < m.RowLength; r++) {
                for (int c = 0; c < m.ColumnLength; c++) {
                    temp.matrix[r, c] = m.matrix[c, r];
                }
            }

            return temp;
        }

        public bool isSquared()
        {
            return RowLength == ColumnLength;
        }

        public void AddExtraDimension()
        {
            // Create temporary matrix with 1 extra dimension
            var temp = new Matrix(RowLength + 1, ColumnLength + 1);

            // Copy the matrix data into the temporary matrix
            for (int r = 0; r < RowLength; r++) { 
                for (int c = 0; c < ColumnLength; c++) { 
                    temp.matrix[r, c] = matrix[r, c]; 
                }
            }

            // set last diagonal number to 1 ( like identity matrix )
            temp.matrix[temp.RowLength-1, temp.ColumnLength-1] = 1;

            matrix = temp.matrix;
        }
        public void AddRow()
        {
            // Create temporary matrix with 1 extra Row
            var temp = new Matrix(RowLength + 1, ColumnLength);

            // Copy the matrix data into the temporary matrix
            for (int r = 0; r < RowLength; r++) {
                for (int c = 0; c < ColumnLength; c++) {
                    temp.matrix[r, c] = matrix[r, c];
                }
            }

            // Add only 1's to last row
            for (int i = 0; i < ColumnLength; i++)
            {
                temp.matrix[RowLength, i] = 1;
            }

            matrix = temp.matrix;

        }
        public void DelRow()
        {
            // Create temporary matrix with 1 less Row
            var temp = new Matrix(RowLength - 1, ColumnLength);

            // Copy the matrix data into the temporary matrix
            for (int r = 0; r < RowLength - 1; r++)
            {
                for (int c = 0; c < ColumnLength; c++)
                {
                    temp.matrix[r, c] = matrix[r, c];
                }
            }

            matrix = temp.matrix;
        }

        public void SwapRow(int r1, int r2)
        {
            var row1 = new double[matrix.GetLength(1)];
            for (int i = 0; i < row1.Length; i++)
            {
                row1[i] = matrix[r1, i];
            }

            for (int j = 0; j < row1.Length; j++)
            {
                matrix[r1, j] = matrix[r2, j];
                matrix[r2, j] = row1[j];
            }
        }

        public int GetHighestRow()
        {
            // METHOD 1 Checks all columns
            /*
            int highestRow = 0;
            double heighestValue = matrix[0, 0];
            for (int j = 0; j < ColumnLength; j++)
            {
                for (int i = 0; i < RowLength; i++)
                {
                    if(matrix[i, j] > heighestValue)
                    {
                        heighestValue = matrix[i, j];
                        highestRow = i;
                    }
                }
            }
            */

            // METHOD 2 Checks first column
            int highestRow = 0;
            double highestValue = matrix[0, 0];
            for (int i = 0; i < RowLength; i++)
            {
                if(matrix[i, 1] > highestValue)
                {
                    highestValue = matrix[i, 1];
                    highestRow = i;
                }
            }

            return highestRow;
        }

        public void MultiplyForRow(double factor, int row)
        {
            for (int j = 0; j < ColumnLength; j++)
            {
                matrix[row, j] *= factor;
            }
        }

        private void EliminateRow(int minuendRow, int subtrahendRow, double factor)
        {
            // Eliminate row by subtracting the subtrahend value times the factor from the minuend value (value you want to eliminate)
            for (int j = 0; j < ColumnLength; j++)
            {
                matrix[minuendRow, j] -= (matrix[subtrahendRow, j] * factor);
            }
        }

        private void EliminateRows(int subtrahendRow, Matrix rhs)
        {
            for (int i = 0; i < RowLength; i++)
            {
                // Check if it's not the row of the value 1
                if(i != subtrahendRow)
                {
                    // Check if the row (i) hasn't already have a 0
                    if(matrix[i, subtrahendRow] != 0)
                    {
                        // Calculate factor by dividing minuend value (value you want to eliminate) by the subtrahend value (which is 1)
                        var subtrahendValue = matrix[subtrahendRow, subtrahendRow];
                        var minuendValue = matrix[i, subtrahendRow];
                        var factor = minuendValue / subtrahendValue;
                        // Eliminate this row using the calculated factor
                        EliminateRow(i, subtrahendRow, factor);
                        // Do the same for the row on the right hand side with the same factor
                        rhs.EliminateRow(i, subtrahendRow, factor);
                    }
                }
            }
        }

        public Matrix Clone()
        {
            // creates new matrix with the same values
            var dup = new Matrix(RowLength, ColumnLength);
            dup.matrix = (double[,])matrix.Clone();
            return dup;
        }

        public static Matrix GetInverseMatrix(Matrix inMatrix)
        {
            // Part 1

            // Check if inMatrix is a square matrix
            if(!inMatrix.isSquared())
            {
                return null;
            }

            var dimensionSize = inMatrix.RowLength;

            // Create matrix on the right hand side
            var rhs = new Matrix(MatrixType.UNITY, dimensionSize);

            // Get row with the highest value
            int highestRow = inMatrix.GetHighestRow();

            // Swap highest value row with the first row 
            inMatrix.SwapRow(0, highestRow);
            rhs.SwapRow(0, highestRow);

            // Part 2 Multiply and eliminate ohter rows
            for (int i = 0; i < dimensionSize; i++)
            {
                // Calculate row using factor that is retrieved from the dimension value
                var mFactor = 1 / inMatrix.matrix[i, i];
                inMatrix.MultiplyForRow(mFactor, i);
                rhs.MultiplyForRow(mFactor, i);

                // Eliminate other rows to get 0
                inMatrix.EliminateRows(i, rhs);
            }

            return rhs;
        }
    }
}
