﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LINAL_Eindopdracht.Utils;
using LINAL_Eindopdracht;

namespace LINAL_UnitTests
{
    [TestClass]
    public class MatrixTest
    {
        public object MatrixController { get; private set; }

        [TestMethod]
        public void AddingTest()
        {
            var A = new Matrix(3, 3);
            var arrA = new double[3, 3]
            {
                { 1, 2, 3 },
                { 4, 5, 6 },
                { 7, 8, 9 },
            };
            A.Copy(arrA);

            var B = new Matrix(3, 3);
            var arrB = new double[3, 3]
            {
                { 7, 8, 9 },
                { 4, 5, 6 },
                { 1, 2, 3 },
            };
            B.Copy(arrB);

            var resultMatrix = new Matrix(3, 3);
            var arrM = new double[3, 3]
            {
                { 8, 10, 12 },
                { 8, 10, 12 },
                { 8, 10, 12 },
            };
            resultMatrix.Copy(arrM);

            // add matrices
            Matrix result = A + B;

            for (int i = 0; i < result.RowLength; i++)
            {
                for (int j = 0; j < result.ColumnLength; j++)
                {
                    Assert.AreEqual(result.ElementAt(i, j), resultMatrix.ElementAt(i, j));
                }
            }
        }

        [TestMethod]
        public void MultiplyTest()
        {
            var A = new Matrix(3, 3);
            var arrA = new double[3, 3]
            {
                { 1, 2, 3 },
                { 4, 5, 6 },
                { 7, 8, 9 },
            };
            A.Copy(arrA);

            var B = new Matrix(3, 3);
            var arrB = new double[3, 3]
            {
                { 7, 8, 9 },
                { 4, 5, 6 },
                { 1, 2, 3 },
            };
            B.Copy(arrB);

            var resultMatrix = new Matrix(3, 3);
            var arrM = new double[3, 3]
            {
                { 18,  24,  30 },
                { 54,  69,  84 },
                { 90, 114, 138 },
            };
            resultMatrix.Copy(arrM);

            // multiply
            Matrix result = A * B;

            for (int i = 0; i < result.RowLength; i++)
            {
                for (int j = 0; j < result.ColumnLength; j++)
                {
                    Assert.AreEqual(result.ElementAt(i, j), resultMatrix.ElementAt(i, j));
                }
            }
        }


        [TestMethod]
        public void InverseTest()
        {
            var matrix = new Matrix(3, 3);
            var arr = new double[3, 3]
            {
                { 0, 1, 2 },
                { 1, 2, 4 },
                { 8, 4, 2 },
            };
            matrix.Copy(arr);

            var inverse = Matrix.GetInverseMatrix(matrix);

            var matrix2 = new Matrix(3, 3);
            var arr2 = new double[3, 3]
            {
                { -2D, 1D, 0D },
                { 5D, -(8/3D), 1/3D },
                { -2D, 4/3D, -1/6D }
            };
            matrix2.Copy(arr2);

            for (int i = 0; i < matrix2.RowLength; i++)
            {
                for (int j = 0; j < inverse.ColumnLength; j++)
                {
                    Assert.AreEqual(matrix2.ElementAt(i, j), inverse.ElementAt(i, j));
                }
            }
        }

        [TestMethod]
        public void ScaleTest()
        {
            Matrix m = new Matrix(3, 4);
            var arr = new double[3, 4]
            {
                { 1, 2, 3 ,0},
                { 4, 5, 6 ,0},
                { 7, 8, 9 ,0},
            };
            m.Copy(arr);
            MatrixController mc = new MatrixController(m);

            // scale
            mc.Scale(true);


            var result = new Matrix(3, 4);
            var arr2 = new double[3, 4]
            {
                { 1.03, 2.06, 3.09 ,0},
                { 4.12, 5.15, 6.18 ,0},
                { 7.21, 8.24, 9.27 ,0},
            };
            result.Copy(arr2);

            var r = mc.getMatrix();

            
            for (int i = 0; i < r.RowLength; i++)
            {
                for (int j = 0; j < r.ColumnLength; j++)
                {
                    Assert.AreEqual(r.ElementAt(i, j), result.ElementAt(i, j));
                }
            }
        }

        [TestMethod]
        public void TranslateTest()
        {
            Matrix m = new Matrix(3, 4);
            var arr = new double[3, 4]
            {
                { 1, 2, 3 ,0},
                { 4, 5, 6 ,0},
                { 7, 8, 9 ,0},
            };
            m.Copy(arr);
            MatrixController mc = new MatrixController(m);

            // scale
            mc.Translate(LINAL_Eindopdracht.Enum.Translate.X, true);

            var result = new Matrix(3, 4);
            var arr2 = new double[3, 4]
            {
                { 2.01, 3.01, 4.01 ,1.01},
                { 4, 5, 6 ,0},
                { 7, 8, 9 ,0},
            };
            result.Copy(arr2);

            var r = mc.getMatrix();
            
            for (int i = 0; i < r.RowLength; i++)
            {
                for (int j = 0; j < r.ColumnLength; j++)
                {
                    Assert.AreEqual(r.ElementAt(i, j), result.ElementAt(i, j));
                }
            }
        }

        [TestMethod]
        public void RotateTest()
        {
            Matrix m = new Matrix(3, 4);
            var arr = new double[3, 4]
            {
                { 1, 2, 3 ,0},
                { 4, 5, 6 ,0},
                { 7, 8, 9 ,0},
            };
            m.Copy(arr);
            MatrixController mc = new MatrixController(m);


            var rot = Matrix.GetXRotationMatrix(1);
            var result = rot * m;

            // scale
            mc.Rotate(1, LINAL_Eindopdracht.Enum.RotateDirection.Up);
            var r = mc.getMatrix();

            for (int i = 0; i < r.RowLength; i++)
            {
                for (int j = 0; j < r.ColumnLength; j++)
                {
                    Assert.AreEqual(r.ElementAt(i, j), result.ElementAt(i, j));
                }
            }
        }
    }
}